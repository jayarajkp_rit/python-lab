# class defintion
class Bank_Account:
    def __init__(self):
        print("Hello!!! Welcome to the Bank_Account:")
        self.balance=0
        self.name=''
        self.acctype=''
        self.account_number=0
# details taking function
    def details(self):
        self.name=input("Enter the name:")
        self.acctype=input("Enter the acctype:")
        self.account_number=input("Enter the accountnumber:")
# deposit function
    def deposit(self):
        amount=float(input("Enter amount to be Deposited: "))
        self.balance += amount
        print("\n Amount Deposited:",amount)
# withdraw function 
    def withdraw(self):
        amount = float(input("Enter amount to be Withdrawn: "))
        if self.balance>=amount:
            self.balance-=amount
            print("\n You Withdraw:", amount)
        else:
            print("\n Insufficient balance  ")
# displaying details 
    def display(self):
        print("Hi",self.name)
        print("Your Accountnumber:",self.account_number)
        print("Your Accounttype:",self.acctype)
        print("\n Available Balance=",self.balance)
 
  
# creating an object of class
s = Bank_Account()
  
# Calling functions with that class object
s.details()
s.deposit()
s.withdraw()
s.display()